﻿using System;

namespace SpesRoboSampleApp
{
    class Config
    {
        public static bool UseKinect = true;
        public static bool ShowGUI = false;
    }
}
