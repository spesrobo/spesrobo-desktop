﻿using System;
using System.Windows.Forms;
using SpesRobo;

namespace SpesRoboSampleApp
{
    class Program
    {
        //[STAThread]
        static void Main(string[] args)
        {
            if (Config.ShowGUI == true)
            {
                /*Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new PlantForm());*/
            }
            else {
                new SpesRobo.SpesRobo();
                Console.WriteLine("INFO: SpesRobo server is running...");
                //Console.WriteLine("Enter q to quit application...");

                while (true)
                {
                    string value = Console.ReadLine();
                    if (value == "q") {
                        break;
                    }
                }
            }
        }
    }
}
