﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Diagnostics;
using System.Threading;
using Fleck;
using Newtonsoft.Json;

namespace SpesRobo
{

    public class Web {
        public delegate void DataReceivedHandler(string obj, string method, dynamic pars);
        public event DataReceivedHandler DataReceived;

        private WebSocketServer ws;
        private IWebSocketConnection socket;

        public Web() {
            ws = new WebSocketServer("ws://0.0.0.0:1017");

            ws.Start(_socket => {
                _socket.OnMessage = message => this.handleData(message);
                //_socket.OnOpen = () => this.send("test");
                this.socket = _socket;
            });
        }

        private void handleData(string data) {
            //Console.Write(data);
            dynamic obj = JsonConvert.DeserializeObject<dynamic>(data);
            DataReceived(Convert.ToString(obj["o"]), Convert.ToString(obj["m"]), obj["p"]);
        }

        public void send(string data) {
            try
            {
                socket.Send(data);
            }catch {}
            }
    }

    /*
    class InternetCommunicator
    {
        public delegate void DataReceivedHandler(string action, List<string> parametres);
        public event DataReceivedHandler DataReceived;

        private HttpListener listener;
        private Thread listenerThread;

        private string dataToResponse = "";

        public InternetCommunicator()
        {
            listener = new HttpListener();

            listener.Prefixes.Clear();
            listener.Prefixes.Add("http://*:8080/");

            try
            {
                listener.Start();
            }
            catch { RestartAsAdmin(); }

            listenerThread = new Thread(HandleRequests);
            listenerThread.Start();

            // Run HTTP Server
            //Process.Start("CMD.exe", "http-server E:/Projects/HarvestDrone/webapp -p 3000 -a 127.0.0.1");
        }

        ~InternetCommunicator()
        {
            listener.Stop();
            listenerThread.Abort();
        }

        public void addData(string data)
        {
            data = data.Replace("\n", "");
            data = data.Replace("\r", "");

            if (dataToResponse != "")
                dataToResponse += ", ";

            dataToResponse += data;
        }

        private void HandleRequests()
        {
            while (listener.IsListening)
            {
                var context = listener.BeginGetContext(new AsyncCallback(ListenerCallback), listener);
                context.AsyncWaitHandle.WaitOne();
            }
        }

        private void ListenerCallback(IAsyncResult ar)
        {
            var listener = ar.AsyncState as HttpListener;
            var context = listener.EndGetContext(ar);

            context.Response.AddHeader("Access-Control-Allow-Origin", "*");

            if (context.Request.RawUrl == "/listen")
            {
                HttpListenerResponse response = context.Response;
                byte[] buffer = System.Text.Encoding.UTF8.GetBytes("[" + dataToResponse + "]");
                response.ContentLength64 = buffer.Length;
                System.IO.Stream output = response.OutputStream;
                output.Write(buffer, 0, buffer.Length);
                output.Close();

                dataToResponse = "";

                context.Response.Close();
                return;
            }

            
            context.Response.Close();

            List<string> data = new List<string>(context.Request.RawUrl.Substring(1).Split('/'));

            if (data.Count >= 2)
            {
                List<string> parametres = new List<string>();
                for (int i = 1; i < data.Count; i++)
                    parametres.Add(data[i]);

                DataReceived(data[0], parametres);
            }
        }

        

        static void RestartAsAdmin()
        {
            var startInfo = new ProcessStartInfo("HarvestDrone.exe") { Verb = "runas" };
            Process.Start(startInfo);
            Environment.Exit(0);
        }
    }
     * */
}
