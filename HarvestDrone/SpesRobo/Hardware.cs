﻿using System;
using System.IO.Ports;
using Microsoft.Kinect;

namespace SpesRobo
{
    public class Hardware
    {
        private SerialPort serialPort;
        private KinectSensor kinect;

        public delegate void DataReceivedHanlder(string data);
        public event DataReceivedHanlder DataReceived;

        private long kinectVerticalAngleLastSet = 0;

        public Hardware()
        {
            // Serial port
            if (SerialPort.GetPortNames().Length > 0)
            {
                serialPort = new SerialPort();
                serialPort.BaudRate = 115200;
                serialPort.DataReceived += serialPort_DataReceived;
                serialPort.PortName = SerialPort.GetPortNames()[0];
                serialPort.Open();
                serialPort.WriteLine("af90as90");

            }
            else {
                Console.WriteLine("WARNING: No Serial Port");
            }
             

            // Kinect
            if (KinectSensor.KinectSensors.Count > 0)
            {
                kinect = KinectSensor.KinectSensors[0];
            }
            else {
                Console.WriteLine("WARNING: No Kinect Sensor");
            }
        }

        void serialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            string data = serialPort.ReadLine();
            Console.WriteLine(data);
            DataReceived(data);
        }

        public void setCommand(string command)
        {
            serialPort.WriteLine(command);
            Console.Write(command+Environment.NewLine);
        }

        public void setArmCoordinates(double x, double y, double z)
        {
            Console.Write(/*x + " " + */y + " " + z + " ");
            serialPort.WriteLine(/*"ax" + (int)x + */"ay" + (int)y + "az" + (int)z);
        }


        public int getKinectAngle()
        {
            return kinect.ElevationAngle;
        }

        public void setKinectAngle(int angle)
        {
            if (kinectVerticalAngleLastSet + 1000 < DateTime.Today.Millisecond)
            {
                kinectVerticalAngleLastSet = DateTime.Today.Millisecond;

                if (angle < getKinectAngleMax() && angle > getKinectAngleMin())
                {
                    try
                    {
                        kinect.ElevationAngle = angle;
                    }
                    catch { }
                }
            }
        }

        public int getKinectAngleMax()
        {
            return kinect.MaxElevationAngle;
        }

        public int getKinectAngleMin()
        {
            return kinect.MinElevationAngle;
        }
    }
}
