﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.UI;
using DirectShowLib;
using Microsoft.Kinect;
using SpesRobo.Helpers;
using Emgu.CV.Cvb;
using System.Diagnostics;

namespace SpesRobo.Plugins
{
    class ColoredObjectDetector
    {
        public int xw, yw, zw;
        public int xv, yv, zv;

        private KinectSensor kinect;
        private Bgr minBgr = new Bgr(33, 20, 110);
        private Bgr maxBgr = new Bgr(83, 70, 166);
        private int minArea = 200;

        private int ignoredFrames = 0;

        public const int zDistanceToArm = 370;
        //public const int yDistanceToArm = 440;
        public const int yDistanceToArm = 250;
        public const int kinectAngleFix = 45;
        public const int yDistanceToKinect = 700;
        public const int xDistanceToKinect = 70;
        public const int yHorizontalDistanceToArm = 360;
        public const int ViewWidth = 240;
        public const int ViewStartX = 200;
        public const int ViewAngle = 33;
        public const double zoom = 0.41;
        private int[] ycoords = new int[5];
        private int[] xcoords = new int[5];
        private int[] zcoords = new int[5];
        private int[] ycoordscopy = new int[5];
        public const bool zoomActive = true;
        int frames = 0;

        ColorImageFrame colorFrame;
        DepthImageFrame depthFrame;
        bool KinectBusy = false;

        public ColoredObjectDetector()
        {
            kinect = KinectSensor.KinectSensors[0];
            kinect.AllFramesReady += Kinect_AllFramesReady;

            kinect.Start();
            kinect.ColorStream.Enable(ColorImageFormat.YuvResolution640x480Fps15);
            kinect.DepthStream.Enable(DepthImageFormat.Resolution640x480Fps30);
        }

        void Kinect_AllFramesReady(object sender, AllFramesReadyEventArgs e)
        {
            if (ignoredFrames < 2)
            {
                ignoredFrames++;
                return;
            }
            ignoredFrames = 0;


            if (KinectBusy == true)
                return;
            colorFrame = e.OpenColorImageFrame();
            depthFrame = e.OpenDepthImageFrame();
            
        }

        void setMinBgr(int blue, int green, int red)
        {
            this.minBgr = new Bgr(blue, green, red);
        }

        void setMaxBgr(int blue, int green, int red)
        {
            this.maxBgr = new Bgr(blue, green, red);
        }

        public Image<Bgr, Byte> DetectObject()
        {
            KinectBusy = true;
            Image<Bgr, Byte> colorImageEmgu;
            Image<Gray, Byte> grayImageEmgu;

            CvBlob biggestCvBlob = null;
            if (colorFrame == null)
            {
                Debug.Write("Null frame"+Environment.NewLine);
                KinectBusy = false;
                return null;
            }

            
                Bitmap colorImage = KinectHelper.ImageToBitmap(colorFrame);
                
                    using (var g = Graphics.FromImage(colorImage))
                    {

                        g.FillRectangle(Brushes.White, new Rectangle(0, 0, ViewStartX, colorImage.Height));
                        g.FillRectangle(Brushes.White, new Rectangle(ViewStartX + ViewWidth, 0, colorImage.Width - (ViewStartX + ViewWidth), colorImage.Height));
                    }

                    colorImageEmgu = new Image<Bgr, Byte>(colorImage);
                    grayImageEmgu = colorImageEmgu.InRange(this.minBgr, this.maxBgr);

                    CvBlobs cvBlobs = new CvBlobs();
                    CvBlobDetector cvBlobDetector = new CvBlobDetector();
                    uint detectedBlobs = cvBlobDetector.Detect(grayImageEmgu, cvBlobs);

                    // Find biggest blob
                    bool first = true;
                    foreach (CvBlob cvBlob in cvBlobs.Values)
                    {
                        if (cvBlob.Area > this.minArea)
                        {
                            if (first == true)
                            {
                                biggestCvBlob = cvBlob;
                                first = false;
                            }
                            if (biggestCvBlob.Area < cvBlob.Area)
                            {
                                biggestCvBlob = cvBlob;
                            }
                        }
                    }

                    // Draw blobs on Color Picture
                    if (biggestCvBlob != null)
                    {
                        colorImageEmgu.Draw(new CircleF(biggestCvBlob.Centroid, (int)Math.Sqrt(biggestCvBlob.Area)), new Bgr(Color.Red), 1);
                        MCvFont f = new MCvFont(Emgu.CV.CvEnum.FONT.CV_FONT_HERSHEY_SIMPLEX, 0.8, 0.1);
                        string text = ((int)biggestCvBlob.Centroid.X).ToString() + " " + ((int)biggestCvBlob.Centroid.Y).ToString();
                        Point point = new Point((int)biggestCvBlob.Centroid.X, (int)biggestCvBlob.Centroid.Y);
                        colorImageEmgu.Draw(text, ref f, point, new Bgr(0, 255, 0));
                    }

            if (biggestCvBlob == null)
            {
                KinectBusy = false;
                return colorImageEmgu;
            }

            if (depthFrame != null && biggestCvBlob.Centroid.X >= 200 && biggestCvBlob.Centroid.X <= 440)
            {
                //Debug.Write("Frames: " + frames + Environment.NewLine);
                this.xv = Convert.ToInt32(biggestCvBlob.Centroid.X);
                this.yv = Convert.ToInt32(biggestCvBlob.Centroid.Y);
                this.zv = KinectHelper.GetDistance(depthFrame, this.xv, this.yv);
                Catch(colorFrame, this.xv, this.yv, this.zv);
            }
            KinectBusy = false;
            return colorImageEmgu;
        }

        void Catch(ColorImageFrame colorFrame, int x, int y, int z)
        {
            // Kinect object tracking is not supported

            this.xv = x;
            this.yv = y;

            this.xv -= ViewStartX;
            this.yv = colorFrame.Height - this.yv;

            int SphereR = colorFrame.Height;
            double SphereAngleX = ((Math.PI * this.xv) / SphereR);
            double SphereAngleY = ((Math.PI * this.yv) / SphereR);

            double ZoomStrength = Math.Abs(Math.Cos(SphereAngleX) * Math.Cos(SphereAngleY));
            double ZoomMag = zoom + ZoomStrength * 0.01;

            if (zoomActive == true)
                this.zv = (int)(z - (z * ZoomMag));
            else
                this.zv = z;

            int horizontalAngle = 43;
            if (zoomActive == true)
                horizontalAngle = 90;

            double ff = (colorFrame.Height / (2 * Math.Tan((horizontalAngle / 2) * (Math.PI / 180))));
            double xalpha = Math.Atan((this.yv - (colorFrame.Height / 2)) / ff);
            this.yw = (int)(Math.Sin(xalpha) * this.zv);
            this.yw += yDistanceToKinect - yDistanceToArm;


            ff = (ViewWidth / (2 * Math.Tan((ViewAngle / 2) * (Math.PI / 180))));
            double xbeta = Math.Atan((this.xv - (ViewWidth / 2)) / ff);
            this.xw = (int)(Math.Sin(xbeta) * this.zv);
            this.xw += xDistanceToKinect;

            this.zw = (int)(Math.Cos(xalpha) * (this.zv - (yHorizontalDistanceToArm / Math.Cos(xalpha))));

            //Debug.WriteLine(this.xw + " " + this.yw + " " + this.zw + Environment.NewLine);
        }

        public int GetObjectX()
        {
            return this.xw;
        }
        public int GetObjectY()
        {
            return this.yw;
        }
        public int GetObjectZ()
        {
            return this.zw;
        }

        public void ChangeColor(int r, int g, int b)
        {
            setMinBgr(b - 20, g - 20, r - 20);
            setMaxBgr(b + 20, g + 20, r + 20);
        }

        public void ChangeAreaSize(int size)
        {
            minArea = size;
            return;
        }
    }
}
