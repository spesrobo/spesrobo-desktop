﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpesRobo.Plugins
{
    /// <summary>
    /// Sample plugin. Another good example is Spesrobo.Plugins.FrontCamera
    /// </summary>

    class Test
    {
        public delegate void SampleEventHanlder(string data);
        public event SampleEventHanlder SampleEvent;

        public Test()
        {
            Console.WriteLine("Object created...");
        }

        public int sampleMethod(double param)
        {
            Console.WriteLine("Sample method " + param + "...");
            SampleEvent(param.ToString());
            return (int)param;
        }
    }
}
