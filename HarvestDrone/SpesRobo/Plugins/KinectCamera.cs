﻿using Microsoft.Kinect;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpesRobo.Plugins
{
    class KinectCamera
    {
        private KinectSensor kinect;
        private ColorImageFrame colorFrame;

        public KinectCamera() {
            kinect = KinectSensor.KinectSensors[0];
            kinect.ColorFrameReady += kinect_ColorFrameReady;

            kinect.Start();
            kinect.ColorStream.Enable(ColorImageFormat.YuvResolution640x480Fps15);
        }

        private void kinect_ColorFrameReady(object sender, ColorImageFrameReadyEventArgs e)
        {
            if (e.OpenColorImageFrame() != null)
            {
                colorFrame = e.OpenColorImageFrame();
            }
        }

        public ColorImageFrame getFrame() 
        {
            return colorFrame;
        }
    }
}
