﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.UI;
using DirectShowLib;

namespace SpesRobo.Plugins
{
    class FrontCamera
    {
        private Capture capture;

        public FrontCamera()
        {
            try
            {
                capture = new Capture(0);
            }
            catch (Exception e) {
                Console.WriteLine("ERROR: Cannot initialize Front Camera");
            }
        }

        public Emgu.CV.Image<Emgu.CV.Structure.Bgr, byte> getFrame() 
        {
            capture.Start();
            var frame = capture.RetrieveBgrFrame();
            capture.Stop();

            return frame;
        }
    }
}
