﻿using System;

namespace SpesRobo.Helpers
{
    class MathHelper
    {
        /// <summary>
        /// Calculates Tan from degrees
        /// </summary>
        public static double Tan(double angleInDegrees)
        {
            return Math.Tan((angleInDegrees * Math.PI) / 180);
        }
    }
}
