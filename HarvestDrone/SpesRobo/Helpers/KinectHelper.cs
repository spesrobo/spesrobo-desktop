﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using Microsoft.Kinect;
using System.Runtime.InteropServices;
using System.IO;

namespace SpesRobo.Helpers
{
    /// <summary>
    /// Methods which are not implemented in Kinect SDK and should be.
    /// </summary>
    static class KinectHelper
    {
        /// <summary>
        /// Convert Kinect's ColorImageFrame to Bitmap
        /// </summary>
        /// <remarks>
        /// Reference: http://stackoverflow.com/questions/10848190/convert-kinect-colorimageframe-to-bitmap
        /// </remarks>
        /// <param name="Image">Kinect's ColorImageFrame</param>
        /// <returns>Bitmap image</returns>
        public static Bitmap ImageToBitmap(ColorImageFrame Image)
        {
            byte[] pixeldata = new byte[Image.PixelDataLength];
            Image.CopyPixelDataTo(pixeldata);
            Bitmap bmap = new Bitmap(Image.Width, Image.Height, PixelFormat.Format32bppRgb);
            BitmapData bmapdata = bmap.LockBits(
                new Rectangle(0, 0, Image.Width, Image.Height),
                ImageLockMode.WriteOnly,
                bmap.PixelFormat);
            IntPtr ptr = bmapdata.Scan0;
            Marshal.Copy(pixeldata, 0, ptr, Image.PixelDataLength);
            bmap.UnlockBits(bmapdata);
            return bmap;
        }

        /// <summary>
        /// Convert Kinect's DepthImageFrame to Bitmap 
        /// </summary>
        /// <param name="imageFrame">Kinect's DepthImageFrame</param>
        /// <returns>Bitmap image</returns>
        public static Bitmap DepthToBitmap(DepthImageFrame imageFrame)
        {
            short[] pixelData = new short[imageFrame.PixelDataLength];
            imageFrame.CopyPixelDataTo(pixelData);
            Bitmap bmap = new Bitmap(imageFrame.Width, imageFrame.Height, PixelFormat.Format16bppRgb565);
            BitmapData bmapdata = bmap.LockBits(new Rectangle(0, 0, imageFrame.Width, imageFrame.Height), ImageLockMode.WriteOnly, bmap.PixelFormat);
            IntPtr ptr = bmapdata.Scan0;
            Marshal.Copy(pixelData, 0, ptr, imageFrame.PixelDataLength);
            bmap.UnlockBits(bmapdata);
            return bmap;
        }

        /// <summary>
        /// Get z axis from Kinect's DepthImageFrame on specific coordinates
        /// </summary>
        /// <remarks>
        /// Reference: http://stackoverflow.com/questions/18765228/how-to-show-kinect-depth-data-in-millimeters
        /// </remarks>
        /// <param name="imageFrame">Kinect's DepthImageFrame</param>
        /// <param name="x">x coordinate of the pixel</param>
        /// <param name="y">y coordinate of the pixel</param>
        /// <returns>z axis in mm</returns>
        public static int GetDistance(DepthImageFrame imageFrame, int x, int y)
        {
            try
            {
                short[] pixelData = new short[imageFrame.PixelDataLength];
                imageFrame.CopyPixelDataTo(pixelData);
                int distance;
                int stretch = 5;
                for (int i = -stretch; i <= stretch; i++)
                {
                    for (int j = -stretch; j <= stretch; j++)
                    {
                        distance = pixelData[x + i + (y + j) * imageFrame.Width] >> DepthImageFrame.PlayerIndexBitmaskWidth;

                        if (distance >= 800 && distance < 1800)
                        {
                            return distance;
                        }
                    }
                }
                return 0;
            }
            catch
            {
                return 0;
            }
        }


        static void WriteShorts(short[] values, string path)
        {
            using (FileStream fs = new FileStream(path, FileMode.OpenOrCreate, FileAccess.Write))
            {
                using (BinaryWriter bw = new BinaryWriter(fs))
                {
                    foreach (short value in values)
                    {
                        bw.Write(value);
                       
                    }
                }
            }
        }


        public static int CreateZoomMatrix(DepthImageFrame imageFrame, int x, int y,int width, int height)
        {
            short[] pixelData = new short[imageFrame.PixelDataLength];
            imageFrame.CopyPixelDataTo(pixelData);
            WriteShorts(pixelData,@"D:\test.dat");


           /* int i, j;
            int CenterZ = GetDistance(imageFrame, imageFrame.Width / 2, imageFrame.Height / 2);
            int zoom;
            int d,w,h;
            int d0, d1;
            w = 0;
            h = 1;
            for (i = y; i < y + height; i++)
            {
                w = 1;
                for (j = x; j < x + width; j++)
                {
                    d0 =  (int)(Math.Sqrt(CenterZ*CenterZ +  (Math.Sqrt((w / 2) * (w / 2) + (h / 2) * (h / 2))*(Math.Sqrt((w / 2) * (w / 2) + (h / 2) * (h / 2))))));
                    d1 = pixelData[i * imageFrame.Width+j] >> DepthImageFrame.PlayerIndexBitmaskWidth;
                    zoom = d0*100 / d1;
                    w++;
                }
                h++;
            }*/

                return 0;
        }

    }


}
