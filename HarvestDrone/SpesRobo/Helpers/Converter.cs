﻿using Microsoft.Kinect;
using System;
using System.Drawing;

namespace SpesRobo.Helpers
{
    /// <summary>
    /// Convert every variable to ASCII array which is able to be transmitted via Websocket.
    /// </summary>
    /// <remarks>
    /// IMPORTANT: If your plugin uses some other data type you must add conversion algorithm here.
    /// </remarks>
    public class WebConverter
    {
        public static string Prepare(object data)
        {
            // Hande null objects
            if (data == null) {
                return "null";
            }

            // Handle special objects
            // IMPORTANT: Add you new data types here
            if (data.GetType().ToString().Contains("CV.Image")) 
            {
                return WebConverter.Prepare((Emgu.CV.Image<Emgu.CV.Structure.Bgr, byte>)data);
            }
            else if (data.GetType().ToString().Contains("ColorImageFrame")) {
                return WebConverter.Prepare(KinectHelper.ImageToBitmap((ColorImageFrame)data));
            }

            // Others
            return data.ToString();
        }

        private static string Prepare(Emgu.CV.Image<Emgu.CV.Structure.Bgr, byte> data)
        {
            Bitmap bitmap = data.ToBitmap();
            return Prepare(bitmap);
        }

        private static string Prepare(Bitmap bitmap) {
            System.IO.MemoryStream ms = new System.IO.MemoryStream();
            bitmap.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
            return "data:image/jpeg;base64," + Convert.ToBase64String(ms.ToArray());
        }
    }
}
