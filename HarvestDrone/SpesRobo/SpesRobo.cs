﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Reflection;
using Newtonsoft.Json;
using SpesRobo.Plugins;
using SpesRobo.Helpers;

namespace SpesRobo
{
    /// <summary>
    /// Class which contains parsed object, method and params transfered via Weboscket.
    /// </summary>
    /// <remarks>
    /// Part of Remote Programming Protocol over Websocket, protocol which developed 
    /// to call methods, create objects, and listen for events in C# via Websocket.
    /// </remarks>
    class ReturnWebObject
    {
        public string o; // Object
        public string m; // Method
        public string r; // Return params
    }


    /// <summary>
    /// Init class. Communication with hardware and web start by initializing this class.
    /// </summary>
    public class SpesRobo
    {
        public Web web = new Web();
        public Hardware hardware = new Hardware();

        private Dictionary<string, object> objects = new Dictionary<string, object>();

        public SpesRobo()
        {
            web.DataReceived += web_DataReceived;

            this.hardware.DataReceived += data =>
            {
                ReturnWebObject retObject = new ReturnWebObject();
                retObject.r = data;
                retObject.o = "Hardware";
                retObject.m = "DataReceived";
                web.send(JsonConvert.SerializeObject(retObject));
            };
        }

        private void callInternalMethod(object obj, string method, dynamic pars, string objname)
        {
            int i = 0;

            ReturnWebObject retObject = new ReturnWebObject();

            List<object> parameters = new List<object>();
            MethodInfo methodInfo = obj.GetType().GetMethod(method);
            ParameterInfo[] methodInfoParams = methodInfo.GetParameters();
            foreach (ParameterInfo methodInfoParam in methodInfoParams)
            {
                object parameter = Convert.ChangeType(pars[i++], methodInfoParam.ParameterType);
                parameters.Add(parameter);
            }

            retObject.r = WebConverter.Prepare(methodInfo.Invoke(obj, parameters.ToArray()));
            retObject.o = objname;
            retObject.m = method;

            web.send(JsonConvert.SerializeObject(retObject));
        }


        void web_DataReceived(string objname, string method, dynamic pars)
        {
            // Objects
            if (objname.Substring(0, 7) == "OBJECT_")
            {
                if (objects.ContainsKey(objname))
                {
                    callInternalMethod(objects[objname], method, pars, objname);
                }
                return;
            }

            // Classes & Static objects
            switch (objname)
            {
                case "Hardware":
                    callInternalMethod(this.hardware, method, pars, "Hardware");
                    break;

                default:
                    if (method == "new")
                    {
                        // Make object instance
                        object instance = Activator.CreateInstance(Type.GetType(objname));
                        objects.Add(pars[0].ToString(), instance);

                        // Register events
                        // Every event should be added inside switch, like SpesRobo.Plugins.Test. 
                        // IMPORTANT: Events are experimenting feature, avoid them if you really don't need!
                        switch (objname) {

                            case "SpesRobo.Plugins.Test":
                                ((Test)instance).SampleEvent += data =>
                                {
                                    ReturnWebObject testRetObject = new ReturnWebObject();
                                    testRetObject.r = data;
                                    testRetObject.o = pars[0].ToString();
                                    testRetObject.m = "SampleEvent";
                                    web.send(JsonConvert.SerializeObject(testRetObject));
                                };
                            break;
                        }
                    }
                break;
            }
        }
    }
}
